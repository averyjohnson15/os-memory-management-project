//for information on program, see README or header comment on oss.cpp

//preprocessor directives
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mqueue.h>
#include <ctime>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

//macro and typedef to be used for sysV queue
#define PERMS 0644
typedef struct msgbuffer
{
	long mtype;
	int intData;
	int pid;
	int rwt;//int will be -1,0,1 for read/write/term respectively
} msgbuffer;

//worker entry
int main(int argc, char* argv[])
{
	//declare resources for mqueue
	msgbuffer sndBuffer, rcvBuffer;
	int msqid = 0;
	key_t key;

	//ftok to get key for queue
	if ((key = ftok("msgq.txt", 1)) == -1)
	{
		printf("issue with ftok childside\n");
		exit(1);
	}

	//join msg queue that parent created
	if ((msqid = msgget(key, PERMS)) == -1)
	{
		printf("issue connecting to queue on childside\n");
		exit(1);
	}	

        //open shared mem sec and nano vars
        //set up sec destination, throw error if issue occurs
        int secMemSeg = shm_open("/secMemSeg", O_RDONLY, 0777);
        if (secMemSeg < 0)
        {
                printf("issue creating sec mem seg\n");
                exit(1);
        }
        //configure size of sec mem seg
        ftruncate(secMemSeg, sizeof(int));
        //use mmap to map sec address, throw relative error if issue occurs
        void* secAddress = mmap(NULL, sizeof(int), PROT_READ, MAP_SHARED, secMemSeg, 0);
        if (secAddress == MAP_FAILED)
        {
                printf("issue setting up sec address\n");
                exit(1);
        }
        //set that address as the value of a pointer to the sec int value
        int* sysSec = (int*)secAddress;

        //set up nano mem seg, throw error if issue occurs
        int nanoMemSeg = shm_open("/nanoMemSeg", O_RDONLY, 0777);
        if (nanoMemSeg < 0)
        {
                printf("Issue creating nano mem seg\n");
                exit(1);
        }
        //configure size of nano mem seg
        ftruncate(nanoMemSeg, sizeof(int));
        //use mmap to map nano address, throw relative error if issue occurs
        void* nanoAddress = mmap(NULL, sizeof(int), PROT_READ, MAP_SHARED, nanoMemSeg, 0);
        if (nanoAddress == MAP_FAILED)
        {
                printf("issue setting up nano address\n");
                exit(1);
        }
        //set shared address as value of pointer to the nano int value
        int* sysNano = (int*)nanoAddress;

	//seed the rng and create max time between subprocess relays back to oss
	srand(time(0) + getpid());

	//int to keep track of how many requests have been made
	int requestsMade = 0;

	//loop through receiving and sending messages to/from OSS until completion
	while (true)
	{
		//declare/calc return address to send back to oss
		int returnAddress = (rand() % 32) * (1024) + (rand() % 1024);

		//declare rand var to be used in return calcs
		int tempRand = rand() % 100 + 1;//read/write/term rand

		//check if should r/w, if requestsMade is at least 1k and +- 100 evenly, add small chance to term
		if (requestsMade >= 1000 && requestsMade % 100 == 0)
		{
			if (tempRand < 75)
				sndBuffer.rwt = -1;
			else if (tempRand < 90)
				sndBuffer.rwt = 0;
			else
				sndBuffer.rwt = 1;
		}
		else
		{
			if (tempRand < 80)
				sndBuffer.rwt = -1;
			else
				sndBuffer.rwt = 0;
		}

		//set rest of sndBuffer attributes
		sndBuffer.mtype = getppid();
		sndBuffer.pid = getpid();
		sndBuffer.intData = returnAddress;

		//attempt to send message to OSS for request, term or return
		if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
		{
			printf("Issue sending message from childside\n");
			exit(1);
		}

		//term after telling oss you're terminating if done so
		if (sndBuffer.rwt == 1)
			exit(0);

		//setup for receive. Block process until it receives message
		if (msgrcv(msqid, &rcvBuffer, sizeof(msgbuffer), getpid(), 0) == -1)
		{
			printf("Issue receiving message on childside\n");
			exit(1);
		}

		//increment request counter ever iteration
		requestsMade++;
	}
        return 0;
}
