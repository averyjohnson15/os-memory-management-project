all: oss user_proc

oss: oss.cpp
	g++ -g -std=c++11 oss.cpp -o oss -lrt

user_proc: user_proc.cpp
	g++ -g -std=c++11 user_proc.cpp -o user_proc -lrt

clean:
	rm oss user_proc
