/***************************************************************************************************
Created by: Avery Johnson
Course/Assignment: CS4760/Project 6
Due Date: 20230509

Notes: See README or help documentation through option -h for further information.

Sources (aside from textbook or man pages):
- None
****************************************************************************************************/

//preprocessor directives
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/types.h>
#include <mqueue.h>
#include <ctime>
#include <queue>
#include <vector>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cmath>
#include <math.h>

using namespace std;

//struct for pages
struct page
{
	int frameNumber;
	int occupied;
} page;
//struct for frames
struct frame
{
	int dirtyBit;
	int occupied;
} frame;

//declare struct prior to prototypes since its used as param
struct PCB
{
	int occupied;
	int pid;
	int startSec;
	int startNano;
	struct page pageTable[32];
};

//struct for sec/nano used in blocked/ready vector totals
struct timePassed
{
	int sec;
	int nano;
};

//typedef and macro for sysV mq
#define PERMS 0644
typedef struct msgbuffer
{
	long mtype;
	int intData;
	int pid;
	int rwt;
} msgbuffer;

//global process tree to be used in signal functions, global ints for mq and file line counter
struct PCB processTable[18];
int msqid;

//prototypes for funcs labeled below main (some funcs below for organizational purposes)
void printProcTable(struct PCB procTable[], int sec, int nano, FILE* logFile);
void removeProcess(struct PCB procTable[], int pid);
void addProcess(struct PCB procTable[], int pid, int sec, int nano);

//function to print the frameTable
void printFrameTable(struct frame frameTable[], int sec, int nano, FILE* logFile, int head)
{
	printf("Current memory layout at %d:%d is:\n", sec, nano);
	fprintf(logFile, "Current memory layout at %d:%d is:\n", sec, nano);
	printf("\t  Occupied\tDirtyBit  HeadOfFIFO\n");
	fprintf(logFile, "\t  Occupied\tDirtyBit  HeadOfFIFO\n");
	for (int i = 0 ; i < 256 ; i++)
	{
		if (i < 10)
		{
			printf("Frame:%d   %d\t\t%d", i, frameTable[i].occupied, frameTable[i].dirtyBit);
			fprintf(logFile, "Frame:%d   %d\t\t%d", i, frameTable[i].occupied, frameTable[i].dirtyBit);
		}
		else if (i < 100)
		{
			printf("Frame:%d  %d\t\t%d", i, frameTable[i].occupied, frameTable[i].dirtyBit);
			fprintf(logFile, "Frame:%d  %d\t\t%d", i, frameTable[i].occupied, frameTable[i].dirtyBit);
		}
		else
		{
			printf("Frame:%d %d\t\t%d", i, frameTable[i].occupied, frameTable[i].dirtyBit);
			fprintf(logFile, "Frame:%d %d\t\t%d", i, frameTable[i].occupied, frameTable[i].dirtyBit);
		}
		if (i == head)
		{
			printf("\t  *\n");
			fprintf(logFile, "\t  *\n");
		}
		else
		{
			printf("\n");
			fprintf(logFile, "\n");
		}
	}
}

//func to find a postion of a given element in the processTable based on its pid
int findPos(struct PCB procTable[], int pid)
{
	int pos = -1;
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].pid == pid)
		{
			pos = i;
			break;
		}
	}
	if (pid == -1)
	{
		printf("issue finding pid pos\n");
		exit(1);
	}
	else
		return pos;
}

//function to find open frame
int findOpenFrame(struct frame frameTable[])
{
	for (int i = 0 ; i < 256 ; i++)
	{
		if (frameTable[i].occupied == 0)
			return i;
	}
	return -1;
}

//function to free up relative frame from fifoQ
queue<int> clearQueuePos(queue<int> q, int frame)
{
	queue<int> temp;

	while (q.size() > 0)
	{
		if (q.front() != frame)
			temp.push(q.front());
		q.pop();
	}
	return temp;
}

//func to call worker
void callWorker()
{
        char* args [] = {"user_proc", 0};
        execv("./user_proc", args);
}

//func to increment time (just for organization)
void incrTime(int* sec, int* nano, int incr)
{
	*nano += incr;
	if (*nano >= 1000000000)
	{
		*nano -= 1000000000;
		(*sec)++;
	}
}

//func that states what to run once SIGINT is caught (ctrl + c)
void sigCatcher(int sig)
{
	//unlink shared mem
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");

        //get rid of sysV msq
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
                printf("issue killing mq on parentside\n");
                exit(1);
        }

	//terminate entire group
	killpg(getpid(), SIGTERM);
}

//code that was given to us for limiting process interval
//this func states what actually happens
static void myhandler(int s) 
{
	//unlink shared mem
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");


        //get rid of sysV msq
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
                printf("issue killing mq on parentside\n");
                exit(1);
        }

	//terminate entire group
	killpg(getpid(),SIGTERM);
}
//this func establishes the interrupt
static int setupinterrupt(void) 
{
	struct sigaction act;
	act.sa_handler = myhandler;
	act.sa_flags = 0;
	return (sigemptyset(&act.sa_mask) || sigaction(SIGPROF, &act, NULL));
	int lastGranted = 0;
}
//this func establishes the timer, started at max 2 seconds, changed to 60 per requirements
static int setupitimer(void) 
{
	struct itimerval value;
	value.it_interval.tv_sec = 60;
	value.it_interval.tv_usec = 0;
	value.it_value = value.it_interval;
	return (setitimer(ITIMER_PROF, &value, NULL));
}

//entry point
int main(int argc, char* argv[])
{
	//declare waitQ to be used to keep track of pids that need to wait in order to sim IO wait times if page fault
	queue<int> waitQ;

	//declare frameTable to be used with pageTables and queue for FIFO page replacement
	struct frame frameTable[256];
	queue<int> fifoQ;

	//fill frameTable with proper empty info
	for (int i = 0 ; i < 256 ; i++)
	{
		frameTable[i].dirtyBit = 0;
		frameTable[i].occupied = 0;
	}

	//DECLARE THE STAT STUFF HERE
	int totalAccesses = 0;
	int totalFaults = 0;

	//calls for error handling in relation to setting the program interval
	if (setupinterrupt() == -1)
	{
		printf("Error setting up interrupt handler, terminating...\n");
		exit(1);
	}
	if (setupitimer() == -1)
	{
		printf("Error setting up interval timer, terminating...\n");
		exit(1);
	}
	//establish what happens if user tries ctrl + c
	signal(SIGINT, sigCatcher);

	//declare resources for mq and touch mq file
	msgbuffer sndBuffer, rcvBuffer;
	key_t key;
	system("touch msgq.txt");

	//ftok to get key for message queue
	if ((key = ftok("msgq.txt", 1)) == -1)
	{
		printf("ftok issue on parentside\n");
		exit(1);
	}	

	//set up msg queue
	if ((msqid = msgget(key, PERMS | IPC_CREAT)) == -1)
	{
		printf("issue with msgget in parent\n");
		exit(1);
	}

	//fill process tree with zeros
	for (int i = 0; i < 18 ;i++)
	{
		processTable[i].occupied = 0;
		processTable[i].pid = 0;
		processTable[i].startSec = 0;
		processTable[i].startNano = 0;
		for (int j = 0; j < 32 ; j++)
		{
			processTable[i].pageTable[j].frameNumber = -1;
			processTable[i].pageTable[j].occupied = 0;
		}
	}

        //declare opt, but define default values for opt args
        int opt;
	string logString = "defaultLog";

        //loop through command line entry until no opts/args remain
        while ((opt = getopt(argc, argv, "hf:")) != -1)
        {
                switch (opt)
                {
                        //incase of help option, show user the help message.
                        case 'h':
                                printf("\n!OSS Help! (Check README for further help information)\n\n");
                                printf("-Please use './oss' for envocation.\n-If you want to set the logFile that the output will be pushed to, an option will need to be used.\n");
                                printf("-An example command is 'oss -f myLog', the command outputs the 'oss' output to the terminal and to the logfile, 'myLog'.\n");
                                printf("\nFor clarity, the options and their meanings are listed below (option -f needs an additional argument):\n");
                                printf("-option '-h' envokes the help portion.\n");
				printf("-option '-f' and its following argument specify the logFile that 'oss' pushes its output to.\n");
                                exit(0);
                        //if proper opt other than -h is used, set the relative variable to the value that is input at the relative arg position
			case 'f':
				logString = optarg;
				break;
                        default:
                                printf("ERROR: An incorrect option was entered, or an argument is missing after an option that expects an argument!\n");
                                exit(1);
                }
        }
	//make FILE* from logString
	FILE* logFile = fopen(logString.c_str(), "w");

	//create shared mem sec and nano vars
	//set up sec destination, throw error if issue occurs
	int secMemSeg = shm_open("/secMemSeg", O_RDWR|O_CREAT, 0777);
	if (secMemSeg < 0)
	{
		printf("issue creating sec mem seg\n");
		exit(1);
	}
	//configure size of sec mem seg
	ftruncate(secMemSeg, sizeof(int));
	//use mmap to map sec address, throw relative error if issue occurs
	void* secAddress = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, secMemSeg, 0);
	if (secAddress == MAP_FAILED)
	{
		printf("issue setting up sec address\n");
		exit(1);
	}
	//set that address as the value of a pointer to the sec int value
	int* sysSec = (int*)secAddress;
	*sysSec = 0;

	//set up nano mem seg, throw error if issue occurs
	int nanoMemSeg = shm_open("/nanoMemSeg", O_RDWR|O_CREAT, 0777);
	if (nanoMemSeg < 0)
	{
		printf("Issue creating nano mem seg\n");
		exit(1);
	}
	//configure size of nano mem seg
	ftruncate(nanoMemSeg, sizeof(int));
	//use mmap to map nano address, throw relative error if issue occurs
	void* nanoAddress = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, nanoMemSeg, 0);
	if (nanoAddress == MAP_FAILED)
	{
		printf("issue setting up nano address\n");
		exit(1);
	}
	//set shared address as value of pointer to the nano int value
	int* sysNano = (int*)nanoAddress;
	*sysNano = 0;

	//ints to keep track of how many children have been terminated, how many are currently running and have been launched
	int terminatedChildren = 0;
	int launchedChildren = 0;
	int maxWorkers = 100; //eventually, 100
	bool maxTimeFlag = false;
	
	//seed the rng and create max time between sec and nano before starting primary loop
	srand(time(NULL));
	int mtbSec = 0;
	int mtbNano = 500000000;

	//create nextTimeFor and nextInterval vars for new process and set initial value
	//manually setting init values to 0 so we start out with 1 process able to run
	int niSec = rand() % (mtbSec + 1);
	int niNano = 0;
	if (niSec == mtbSec)
		niNano = rand() % (mtbNano + 1);
	else
		//should be (999999999 + 1) if mtbSec is above 0
		niNano = rand() % (500000000 + 1);
	int ntfSec = 0;//niSec;
	int ntfNano = 0;//niNano;

	//set start time for 
	time_t loopStart = time(0);

	//last printedTables
	int lastPrint = -1;

	//primary sys loop, stop looping when all necessary children have terminated
	while (terminatedChildren < maxWorkers)
	{
		//check every loop if we need to cap max workers
		if (!maxTimeFlag && time(0) - loopStart >= 2)
		{
			maxTimeFlag = true;
			maxWorkers = launchedChildren;
		}		

		//increment time every loop by a small amount to simulate dispatching operations
		incrTime(sysSec, sysNano, 100000);

		//make sure to set the "head" to -1 if empty fifoQ
		int head;
		if (!fifoQ.size() > 0)
			head = -1;
		else
			head = fifoQ.front();
	
		//print the procTable and frameTable once every sim second
		if (*sysSec > lastPrint)
		{
			lastPrint++;
			printProcTable(processTable, *sysSec, *sysNano, logFile);
			printFrameTable(frameTable, *sysSec, *sysNano, logFile, head);
		}

		//generate new proc if proc needs launched and enough time has passed
		if ((*sysSec > ntfSec || *sysSec == ntfSec && *sysNano >= ntfNano) && launchedChildren < maxWorkers && processTable[17].occupied == 0)
		{
			launchedChildren++;
			//roll new interval/next times
			if (niSec == mtbSec)
				niNano = rand() % (mtbNano + 1);
			else
				niNano = rand() % (500000000 + 1);
			ntfSec = (*sysSec) + niSec;
			ntfNano = (*sysNano) + niNano;
			if (ntfNano >= 1000000000)
			{
				ntfSec++;
				ntfNano -= 1000000000;
			}
			
			//increase count for Lchildren and fork/exec child
			int forkPid = fork();
			//create worker if child
			if (forkPid == 0)
				callWorker();
			else if(forkPid == -1)
			{
				printf("Issue Forking\n");
				exit(1);
			}
			else
			{
				//add to table
				addProcess(processTable, forkPid, *sysSec, *sysNano);
			}
		}
		//init intData with a -1 to check if a message was actually received or running old value at each rcv
		rcvBuffer.rwt = -2;

		//check if any messages need to be received by parentside
		if (msgrcv(msqid, &rcvBuffer, sizeof(msgbuffer), getpid(), IPC_NOWAIT) == -1)
		{
			if (errno != ENOMSG)
			{
				printf("issue receiving parentside\n");
				exit(1);
			}
		}

		//if rwt is -2 then no message was in the queue for the parent
		if (rcvBuffer.rwt == -2)
		{
			//here for testing, currently commented out unless needed
			//printf("NO MESSAGE IN QUEUE FOR PARENT\n");
		}
		//if msg is rcv'd then get pid pos in procTable and proceed accordingly
		else
		{
			//get pos of pid from rcv and set that pid as the mtype
			sndBuffer.mtype = rcvBuffer.pid;
			int pos = findPos(processTable, rcvBuffer.pid);

			//then check if it's a request for r/w/t, proceed accordingly (-1 is read, 0 write, 1 term)
			//first check if request for read/write
			if (rcvBuffer.rwt == -1 || rcvBuffer.rwt == 0)
			{
				//increment the number of totalAccesses no matter happens next
				totalAccesses++;

				//universal prints for r/w
				if (rcvBuffer.rwt == -1)
				{
					printf("Master: %d requesting read of address %d at time %d:%d\n", rcvBuffer.pid, rcvBuffer.intData, *sysSec, *sysNano);
					fprintf(logFile, "Master: %d requesting read of address %d at time %d:%d\n", rcvBuffer.pid, rcvBuffer.intData, *sysSec, *sysNano);
				}
				else
				{
					printf("Master: %d requesting write of address %d at time %d:%d\n", rcvBuffer.pid, rcvBuffer.intData, *sysSec, *sysNano);
					fprintf(logFile, "Master: %d requesting write of address %d at time %d:%d\n", rcvBuffer.pid, rcvBuffer.intData, *sysSec, *sysNano);
				}

				int pagePos = floor(rcvBuffer.intData / 1024);
				//check if the page is in mem already, if not then put it in mem
				if (processTable[pos].pageTable[pagePos].occupied == 0)
				{
					//first, add rcv pid to wait list if this conditional met since IO will be needed due to page fault
					waitQ.push(rcvBuffer.pid);

					//if this happens, some variation of a page fault has occured, increment total number of page faults
					totalFaults++;
					//set address for easier logging
					int addr = rcvBuffer.intData;
					int newPid = rcvBuffer.pid;

					//first get next open frame if possible
					int frame = findOpenFrame(frameTable);
					//then if there is an open frame, add that page to the relative frame
					if (frame != -1)
					{
						//logging
						incrTime(sysSec, sysNano, 800000);
						printf("Master: Address %d not in frame, page fault. Frame %d open, filling with relative page at time %d:%d\n", addr, frame, *sysSec, *sysNano);
						fprintf(logFile, "Master: Address %d not in frame, page fault. Frame %d open, filling with relative page at time %d:%d\n", addr, frame, *sysSec, *sysNano);

						processTable[pos].pageTable[pagePos].occupied = 1;
						processTable[pos].pageTable[pagePos].frameNumber = frame;
						frameTable[frame].occupied = 1;
						fifoQ.push(frame);
						if (rcvBuffer.rwt == 0)
							frameTable[frame].dirtyBit = 1;
					}
					else//page replacement ensues
					{
						bool fifoFound = false;
						//find the page that needs to be replaced
						for (int i = 0 ; i < 18 ; i++)
						{
							for (int j = 0 ; j < 32 ; j++)
							{
								
								if (processTable[i].pageTable[j].frameNumber == fifoQ.front())
								{
									int oldPid = processTable[i].pid;
									//set fifoQ flag
									fifoFound = true;

									//logging information for page swapping
									printf("Master: Address %d not in frame, page fault. Head frame is %d\n", addr, fifoQ.front());
									printf("Master: clearing head Frame. Swapping %d page in\n", newPid);
									fprintf(logFile, "Master: Address %d not in frame, page fault. Head frame is %d\n", addr, fifoQ.front());
									fprintf(logFile, "Master: clearing head Frame. Swapping %d page in\n", newPid);
									if (frameTable[fifoQ.front()].dirtyBit == 1)
									{
										printf("Master: Dirty bit of frame %d set, adding additional time to clock\n", fifoQ.front());
										printf("Master: Indicating to %d that write has happened at previous occupying page\n", oldPid);
										fprintf(logFile, "Master: Dirty bit of frame %d set, adding additional time to clock\n", fifoQ.front());
										fprintf(logFile, "Master: Indicating to %d that write has happened at previous occupying page\n", oldPid);
										incrTime(sysSec, sysNano, 1500000);
									}
									else
										incrTime(sysSec, sysNano, 1000000);

									//clear out pageTable at pos
									processTable[i].pageTable[j].frameNumber = -1;
									processTable[i].pageTable[j].occupied = 0;

									//update new pageTable info
									processTable[pos].pageTable[pagePos].occupied = 1;
									processTable[pos].pageTable[pagePos].frameNumber = fifoQ.front();

									//update frame dirty bit if new request is a write, otherwise, reset to 0
									if (rcvBuffer.rwt = 0)
										frameTable[fifoQ.front()].dirtyBit = 1;
									else
										frameTable[fifoQ.front()].dirtyBit = 0;
									

									//move frame from front to back of fifoQ
									fifoQ.push(fifoQ.front());
									fifoQ.pop();
									break;
								}
							}
							if (fifoFound)
								break;
						}
						if (!fifoFound)
						{
							
							printf("Something wrong with FIFO Q, Head is: %d\n", fifoQ.front());
							exit(1);
						}
					}
				}
				else//else if it is, just check if a write's occurring. If so, set dirty bit
				{
					//if in frame, just send msg immediately to restart since no IO needed
					if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
					{
						printf("Issue sending message on parentside\n");
						exit(1);
					}

					//after, perform logging and set dirty bit if need be
					printf("Master: Address %d in frame %d, giving data to %d at time %d:%d\n", rcvBuffer.intData, processTable[pos].pageTable[pagePos].frameNumber, rcvBuffer.pid, *sysSec, *sysNano);
					fprintf(logFile, "Master: Address %d in frame %d, giving data to %d at time %d:%d\n", rcvBuffer.intData, processTable[pos].pageTable[pagePos].frameNumber, rcvBuffer.pid, *sysSec, *sysNano);
					if (rcvBuffer.rwt == 0)
					{
						incrTime(sysSec, sysNano, 120000);
						frameTable[processTable[pos].pageTable[pagePos].frameNumber].dirtyBit = 1;
					}
					else
						incrTime(sysSec, sysNano, 100000);
				}
			}
			//last, check if it's terming. If so release its mem, remove from table and wait
			else if (rcvBuffer.rwt == 1)
			{
				printf("Master: %d terminating at %d:%d. Clearing out relative frames...\n", processTable[pos].pid, *sysSec, *sysNano);
				fprintf(logFile, "Master: %d terminating at %d:%d. Clearing out relative frames...\n", processTable[pos].pid, *sysSec, *sysNano);
				//check and see if any of the child's pages are active, if so, clear out those frames and remove them from fifoQ
				for (int i = 0 ; i < 32 ; i++)
				{
					if (processTable[pos].pageTable[i].occupied == 1)
					{
						incrTime(sysSec, sysNano, 100000);
						printf("Master: Clearing out frame %d...\n", processTable[pos].pageTable[i].frameNumber);
						fifoQ = clearQueuePos(fifoQ, processTable[pos].pageTable[i].frameNumber);
						frameTable[processTable[pos].pageTable[i].frameNumber].occupied = 0;
						frameTable[processTable[pos].pageTable[i].frameNumber].dirtyBit = 0;
					}
				}
				//add to term, remove from table, wait, etc.
				terminatedChildren++;
				removeProcess(processTable, rcvBuffer.pid);
				wait(NULL);
			}
			else
			{
				printf("error checking intData on parentSide\n");
				exit(1);
			}
		}
		//after main while iteration, check if there's items in waitQ, if so then restart oneevery iteration to sim IO wait times
		if (waitQ.size() > 0)
		{
			sndBuffer.mtype = waitQ.front();
			if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
			{
				printf("Issue sending message parentside waitQ\n");
				exit(1);
			}
			waitQ.pop();
		}
	}

	//display both tables
	printf("\nTABLE AT TERMINATION:\n");
	fprintf(logFile, "TABLE AT TERMINATION:\n");
	printProcTable(processTable, *sysSec, *sysNano, logFile);
	printFrameTable(frameTable, *sysSec, *sysNano, logFile, fifoQ.front());

	//calculate/display relative statistics
	double accessesPerSec = (double)totalAccesses / *sysSec;
	double faultsPerAccess = (double)totalFaults / totalAccesses;
	double avgAccessSpeed = (double)*sysSec / totalAccesses;

	printf("\nTotal Sub-Processes Processed: %d\n", launchedChildren);
	fprintf(logFile, "\nTotal Sub-Processes Processed: %d\n", launchedChildren);
	printf("Total Accesses: %d\t\t\tTotal Faults: %d\n", totalAccesses, totalFaults);
	fprintf(logFile, "TotalAccesses: %d\t\t\tTotalFaults: %d\n", totalAccesses, totalFaults);
	printf("Accesses Per Second: %f\tFaults Per Access: %f\n", accessesPerSec, faultsPerAccess);
	fprintf(logFile, "Accesses Per Second: %f   Faults Per Access: %f\n", accessesPerSec, faultsPerAccess);
	printf("Average Access Speed: %f second(s) for each access\n", avgAccessSpeed);
	fprintf(logFile, "Average Access Speed: %f second(s) for each access\n", avgAccessSpeed);

	//get rid of sysV msq
	if (msgctl(msqid, IPC_RMID, NULL) == -1)
	{
		printf("issue killing mq on parentside\n");
		exit(1);
	}

	//unlink shared memory
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");

	//close logfile
	fclose(logFile);

        return 0;
}

//function to loop through process table and print it out in a more readable manner
//fileprints are just commented out incase you want them in your file
void printProcTable(struct PCB procTable[], int sec, int nano, FILE* logFile)
{
	printf("OSS PID:%d SysClock: %d SysClockNano: %d\n", getpid(), sec, nano);
	fprintf(logFile, "OSS PID:%d SysClock: %d SysClockNano: %d\n", getpid(), sec, nano);
	printf("Process Table:\n");
	fprintf(logFile, "Process Table:\n");
	printf("Entry\tOccupied PID\tActive Pages\tStartS\tStartN\n");
	fprintf(logFile,"Entry\tOccupied PID\tActive Pages\tStartS\tStartN\n");
	for (int i = 0 ; i < 18 ; i++)
	{
		int pageCount = 0;
		for (int j = 0; j < 32 ; j++)
		{
			if (procTable[i].pageTable[j].occupied == 1)
				pageCount++;
		}
		printf("%d\t %d\t %d\t%d\t\t%d\t%d\n", i, procTable[i].occupied, procTable[i].pid, pageCount, procTable[i].startSec, procTable[i].startNano);
		fprintf(logFile, "%d\t %d\t %d\t%d\t\t%d\t%d\n", i, procTable[i].occupied, procTable[i].pid, pageCount, procTable[i].startSec, procTable[i].startNano);
	}
}

//function that loops through the process table to find a relative pid and remove it's line while moving the rest back together
void removeProcess(struct PCB procTable[], int pid)
{
	bool found = false;
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].pid == pid)
		{
			//if assessing the final position of the array, zero it out
			if (i == 17)
			{
				procTable[i].occupied = 0;
				procTable[i].pid = 0;
				procTable[i].startSec = 0;
				procTable[i].startNano = 0;
				for (int j = 0 ; j < 32 ; j++)
				{
					procTable[i].pageTable[j].occupied = 0;
					procTable[i].pageTable[j].frameNumber = -1;
				}
				return;
			}
			//otherwise, set the positional value equal to the next indexed value, moves everything to the "left" by one.
			else
			{
				for (int j = i ; j < 17 ; j++)
				{
					procTable[j].occupied = procTable[j + 1].occupied;
					procTable[j].pid = procTable[j + 1].pid;
					procTable[j].startSec = procTable[j + 1].startSec;
					procTable[j].startNano = procTable[j + 1].startNano;
					for (int k = 0 ; k < 32 ; k++)
					{
						procTable[j].pageTable[k].occupied = procTable[j + 1].pageTable[k].occupied;
						procTable[j].pageTable[k].frameNumber = procTable[j + 1].pageTable[k].frameNumber;
					}
				}
				procTable[17].occupied = 0;
				procTable[17].pid = 0;
				procTable[17].startSec = 0;
				procTable[17].startNano = 0;
				for (int j = 0 ; j < 32 ; j++)
				{
					procTable[17].pageTable[j].frameNumber = -1;
					procTable[17].pageTable[j].occupied = 0;
				}
				return;
			}
		}
	}
}

//function to go to lowest empty position in the process table and enter the relative child information
void addProcess(struct PCB procTable[], int pid, int sec, int nano)
{
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].occupied == 0)
		{
			procTable[i].occupied = 1;
			procTable[i].pid = pid;
			procTable[i].startSec = sec;
			procTable[i].startNano = nano;
			return;
		}
	}
}
